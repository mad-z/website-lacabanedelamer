---
title: Environnement autour de la cabane de la mer, l'Aiguillon-sur-mer, Sud Vendée
description: |
  La cabane de la mer est située dans un environnement très diversifié,
  et adapté aux humeurs et envie de chacun !
---

# L'environnement

La cabane de la mer est située dans un environnement très diversifié,
et adapté aux humeurs et envie de chacun !

## L’Aiguillon sur mer

Situé à l’embouchure du Lay, petit fleuve du sud Vendée, 
le village est né d’une rencontre entre les marais et la mer.

**La pointe de l’Aiguillon** offre une grande richesse paysagère avec alternance de dunes, présalés, marais;
la baie de l’Aiguillon étant une zone migratoire remarquable et un important centre mytilicole et ostréicole.

De l’autre côté du Lay, **La Faute sur mer** se prolonge 
par **la pointe d’Arçay**, bordée de dunes couvertes de forêts.
Une très longue plage a gardé son caractère sauvage et peu construit.
