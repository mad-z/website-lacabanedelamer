module.exports = {
  title: 'La cabane de la mer',
  description: 'Villa à louer à l\'Aiguillon sur mer',
  // permalink: '/:year/:month/:day/:slug',
  themeConfig: {
    nav: [
      { text: 'La cabane', link: '/lacabane.html' },
      { text: 'L\'environnement', link: '/environnement.html' },
      { text: 'Les activités', link: '/activites.html' },
      { text: 'Disponibilités', link: '/disponibilites.html' },
      { text: 'Réservation', link: '/reservation.html' },
    ],
  },
  plugins: [
    [
      'sitemap',
      {
        hostname: 'https://www.lacabanedelamer.fr',
        exclude: ['/404.html', '/message-envoye.html']
      }
    ],
    [
      'vuepress-plugin-medium-zoom',
      {
        // selector: '.my-wrapper .my-img',
        delay: 1000,
        options: {
          margin: 24,
          background: '#ffffffaa',
          scrollOffset: 0,
        },
      }
    ],
    require('./plugins/netlify-cms.js')
  ]
}
