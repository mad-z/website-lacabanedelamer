const fs = require('fs')
const util = require('util')
const { logger } = require('@vuepress/shared-utils')

const readdir = util.promisify(fs.readdir)
const readFile  = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)

function getNetlifyAdmin(cssFilePath) {
  return `
<!doctype html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Gestionnaire de contenu pour la cabane de la mer</title>
  </head>
  <body>
    <!-- Include the script that builds the page and powers Netlify CMS -->
    <script src="https://unpkg.com/decap-cms@^3.0.0/dist/decap-cms.js"></script>
    <script>
      CMS.registerPreviewStyle("${cssFilePath}");
    </script>
  </body>
</html>
  `
}

module.exports = (options, ctx) => {

  return {
    name: 'netlify-cms-admin',
    async generated() {
      logger.info('[netlify-cms-admin] generating netlify admin index/css')
      logger.debug('[netlify-cms-admin] searching for assets css...')
      const files = await readdir(ctx.outDir + '/assets/css')
      let cssData = ''
      logger.debug('[netlify-cms-admin] find ' + files.length + ' file(s), concatenating them...')
      const parsingPromises = files.map(async f => {
        const currentData = await readFile(ctx.outDir + '/assets/css/' + f)
        const currentDataString = currentData.toString()
        cssData += currentDataString + '\n'
      })
      await Promise.all(parsingPromises)
      logger.debug('[netlify-cms-admin] adding a rule for .frame-content css rule (netlify admin preview container)')
      logger.debug('[netlify-cms-admin] searching for max-width in css file concatenated')
      let maxWidth = '100%'
      const searchPattern = '.theme-default-content:not(.custom){max-width:'
      const indexOfSearchPattern = cssData.indexOf(searchPattern)
      if (indexOfSearchPattern > -1) {
        logger.debug('[netlify-cms-admin] found !')
        const beginIndex = indexOfSearchPattern + searchPattern.length
        const endIndex = cssData.indexOf(';', indexOfSearchPattern)
        maxWidth = cssData.substring(beginIndex, endIndex)
      }
      logger.debug('[netlify-cms-admin] value get : ' + maxWidth)
      /**
       * Add specific width according to theme-default-content
       */
      cssData += `
      .frame-content {
        max-width: ${maxWidth};
        width: ${maxWidth};
        margin: auto;
      }
      img {
        max-width: 100%;
      }
      `
      logger.debug('[netlify-cms-admin] writing /admin/preview-styles.css file...')
      await writeFile(ctx.outDir + '/admin/preview-styles.css', cssData)
      logger.debug('[netlify-cms-admin] writing /admin/index.html file...')
      await writeFile(ctx.outDir + '/admin/index.html', getNetlifyAdmin('/admin/preview-styles.css'))
      logger.info('[netlify-cms-admin] files generated !')
    }
  }
}

