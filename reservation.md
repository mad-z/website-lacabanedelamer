---
title: Page de contact / réservation de la cabane de la mer, l'Aiguillon-sur-mer, Sud Vendée
description: |
  Pour nous contacter, par téléphone, email ou via notre formulaire de contact.
---
# Contact / Réservation

### Contactez-nous

**Jacqueline et Jean CHARBONNEAU**

*Tél* : [**06 60 17 35 36**](tel:+33660173536)

*Tél* : [**06 70 71 13 07**](tel:+33670711307)

*mail* : [**jjcharbonneau** [at] **wanadoo.fr**](mailto:jjcharbonneau@wanadoo.fr)

### Envoyez nous un message

Pour toute demande de réservation,
merci de préciser vos coordonnées pour que nous puissions vous répondre
ainsi que la période de réservation souhaitée.

N'hésitez pas à vérifier [**la disponibilité**](./disponibilites)
pour être sûr que la période souhaitée n'est pas déjà réservée.

<form
  name="contact"
  method="POST"
  data-netlify="true"
  data-netlify-honeypot="bot-field"
  action="/message-envoye.html"
>
  <input type="hidden" name="form-name" value="contact" />
  <input type="hidden" name="subject" value="Nouvelle demande de réservation pour la cabane de la mer">
  <div class="d-flex my-form">
    <label class="d-block flex-basis-25">Nom</label>
    <input type="text" name="name" required class="d-block flex-basis-25"/>
    <label class="d-block flex-basis-25">E-mail</label>
    <input type="email" name="email" required class="d-block flex-basis-25"/>
  </div>
  <div class="d-flex my-form">
    <label class="d-block flex-basis-25">Début</label>
    <input type="date" name="debut" class="d-block flex-basis-25" />
    <label class="d-block flex-basis-25">Fin</label>
    <input type="date" name="fin" class="d-block flex-basis-25"/>
  </div>
  <p>
    <label>Message</label>
    <textarea name="message" rows="10" :style="{width: '100%'}"></textarea>
  </p>
  <p class="center">
    <button type="submit">Envoyer !</button>
  </p>
</form>
