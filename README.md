---
title: La cabane de la mer, location de villa de charme à l'Aiguillon-sur-mer,
  Sud Vendée
description: >
  Location de villa / maison de charme à l'Aiguillon-sur-mer en Sud Vendée.

  La cabane de la mer est une maison contemporaine de 4 chambres et 3 salle d'eau

  disponible à la location toute l'année.
backgroundImage: /medias/20220717_191827.jpg
mainTitle: La cabane de la mer
subTitle: Location de villa de charme à l'Aiguillon-sur-mer
---

<div
  class="full-bleed"
  :style="{
    'background-color': '#445568',
    'min-height': '40rem',
    'background': 'url(' + $page.frontmatter.backgroundImage + ')',
    'background-size': 'cover',
    'background-position': 'center',
    'background-repeat': 'no-repeat',
    'color': 'white',
    'display': 'flex',
    'justify-content': 'center',
    'flex-direction': 'column',
  }">
  <h1 :style="{
      'margin-top': '4rem',
      'font-size': '4rem',
      'text-align': 'center',
      'line-height': '4.5rem',
      'text-shadow': '2px 2px 2px #333',
      'border': 'unset'
  }">
    {{ $page.frontmatter.mainTitle }}
  </h1>
  <h2 :style="{
      'margin-top': '4rem',
      'text-align': 'center',
      'font-size': '2.5rem',
      'line-height': '3.5rem',
      'text-shadow': '2px 2px 2px #333'
  }">
    {{ $page.frontmatter.subTitle }}
  </h2>
</div>

# La cabane de la mer

### Location de villa de charme à l'Aiguillon-sur-mer, Sud Vendée

Bienvenue dans votre nouvelle résidence de vacances...

**La cabane de la mer** est une maison contemporaine
comprenant 4 chambres avec 3 salles d’eau, au calme, en plein centre du village de [l'Aiguillon-sur-mer](https://www.laiguillonsurmer.fr/),
proche de la mer et disponible à la [**location toute l'année**](./contact) pour votre plus grand plaisir.

C'est une maison de vacances idéale pour se retrouver en famille ou entre amis, 
dans un authentique village du sud Vendée, 
au centre d’un triangle La Rochelle, Les Sables d’Olonne et l’île de Ré, 
à proximité immédiate de son port de pêche et de grandes plages gardant leur caractère sauvage.

[Découvrir la maison](./lacabane)

Dans un rayon de 60 kilomètres, [La Rochelle](http://www.ville-larochelle.fr/), ville médiévale, 
[Les Sables d’Olonne](https://www.lessablesdolonne.fr/), station balnéaire, le [Marais Poitevin](http://www.maraispoitevin-vendee.com/)...

Le [Puy du Fou](https://www.puydufou.com/),
son grand parcours et sa cinéscénie sont accessibles à 1 heure de voiture.

En apprendre plus sur [les environs](./environnement)
et sur [les activités](./activites).

N'hésitez pas à vérifier [les disponibilités](./disponibilites)
pour [réserver ou prendre contact avec nous](./contact) !

<iframe
  width="750" height="420"
  src="https://www.youtube-nocookie.com/embed/NKrmM3jMeIs"
  frameborder="0"
  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen
></iframe>
