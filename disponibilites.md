---
title: Disponibilités de la cabane de la mer, location de villa de charme à
  l'Aiguillon-sur-mer, Sud Vendée
description: Calendrier des disponibilités pour la location de la cabane de la mer
---
# Disponibilités

N'hésitez pas à réserver via notre [formulaire de contact](./contact.html) !

Autres informations :

* caution : 600 euros
* location draps, linge de toilette et de maison : 15 euros par personne
* forfait ménage ( hors cuisine ) : 90 euros

<center>
<iframe
    src="https://www.planning-planning.com/IFR-Planning-FR-skaftx.htm"
    scrolling="no" width="594px" height="560px" marginwidth="0" marginheight="0" frameborder="0"
    :style="{'max-width': 'unset'}"
></iframe>
</center>

## Tarifs

<center>
    <iframe src="https://www.planning-planning.com/IFR-Planning-FR-skaftx-Tarifs.htm"
      scrolling="no" width="450px" height="151px" marginwidth="0" marginheight="0" frameborder="0"
      :style="{'max-width': 'unset'}"
    ></iframe>
</center>