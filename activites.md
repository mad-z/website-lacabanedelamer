---
title: Activités à proximité de la cabane de la mer, l'Aiguillon-sur-mer, Sud Vendée
description: |
  La cabane de la mer est en plein centre du village, à 800 mètres du port,
  1,8 km  de la plage principale et à proximité immédiate de tout commerce.
---
# Activités

La cabane de la mer est en plein centre du village, à 800 mètres du port,
1,8 km  de la plage principale et à proximité immédiate de tout commerce.

À vélo, il est possible d’accéder rapidement à tous les commerces
et toutes les activités proposées sans utiliser votre voiture.

La plupart des activités liées à la mer sont praticables localement :

* école de voile FFV
* char à voile FFV
* canoé et paddling
* promenade en mer
* pêche en mer
* traversée pour l’île de Ré
* [Golf 9 trous à La Faute sur mer](https://www.golfdelapresquile.fr/)
* Wake-Park  à 300 mètres de la maison

![wake park ( ski nautique tracté )](/medias/703bbc33114b3a090d3d483d2e1-1.jpg "wake parc de l'aiguillon")

![catamarans ecole FFV](/medias/dsc01471.jpg "stages de catamarans")

Il existe plusieurs points d’observation des oiseaux, aménagés,
aussi bien à la pointe d’Arçay que dans la baie de l’Aiguillon.

Les circuits de randonnées pédestres et cyclistes permettent de se déplacer sans risques.
